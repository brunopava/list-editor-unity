﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(ListDisplayer))]
public class ListDisplayerEditor : Editor 
{
	ListDisplayer _target;
	private CustomClass _current;

	public override void OnInspectorGUI()
	{
		_target = (ListDisplayer) target;

		if(GUILayout.Button("Add New"))
		{
			CustomClass temp = new CustomClass();
			_target.exampleList.Add(temp);
		}

		for(int i = 0; i<_target.exampleList.Count; i++)
		{
			_current = _target.exampleList[i];

			EditorGUILayout.BeginHorizontal();
			if(GUILayout.Button("Expand"))
			{
				_current.toggle = !_current.toggle;
			}
		
			_current.toggle = EditorGUILayout.Foldout(_current.toggle, "Custom Class"+(i+1).ToString());
			if(GUILayout.Button("Remove"))
			{
				_target.exampleList.Remove(_current);
				break;
			}

			EditorGUILayout.EndHorizontal();


			if(_current.toggle)
			{
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();

				DrawIntField();

				EditorGUILayout.Separator();

				DrawIntSlider();

				EditorGUILayout.Separator();

				DrawStringField();

				EditorGUILayout.Separator();

				DrawBoolField();

				EditorGUILayout.Separator();

				DrawColorField();
			}
			_current = null;
		}

		if(GUI.changed)
			EditorUtility.SetDirty(target);
	}

	public void DrawIntField()
	{
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Int Example");
		_current.intExample = EditorGUILayout.IntField(_current.intExample);
		EditorGUILayout.EndHorizontal();
	}

	public void DrawIntSlider()
	{
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Int Slider Example");
		_current.sliderExample = EditorGUILayout.IntSlider(_current.sliderExample,1, 10);
		EditorGUILayout.EndHorizontal();
	}

	public void DrawStringField()
	{
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("String Example");
		_current.stringExample = EditorGUILayout.TextField(_current.stringExample);
		EditorGUILayout.EndHorizontal();
	}

	public void DrawBoolField()
	{
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Toggle Example");
		_current.booleanExample = EditorGUILayout.Toggle(_current.booleanExample);
		EditorGUILayout.EndHorizontal();
	}

	public void DrawColorField()
	{
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Color Example");
		_current.colorExample = EditorGUILayout.ColorField(_current.colorExample);
		EditorGUILayout.EndHorizontal();
	}
}
