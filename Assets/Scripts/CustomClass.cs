﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CustomClass 
{
	public int intExample;
	public int sliderExample;
	public string stringExample;
	public Color colorExample;
	public bool booleanExample;


	public bool toggle;
}
